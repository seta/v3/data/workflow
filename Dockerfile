FROM apache/airflow:slim-2.9.2-python3.10

COPY requirements.txt .
RUN pip install --no-cache-dir apache-airflow[celery,postgres,redis,docker]==${AIRFLOW_VERSION} -r requirements.txt

COPY --chown=airflow:root ./dags /opt/airflow/dags