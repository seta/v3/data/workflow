"""
Check docker operator documentation: 
https://airflow.apache.org/docs/apache-airflow-providers-docker/stable/_api/airflow/providers/docker/operators/docker/index.html
"""

from datetime import datetime, timedelta
from airflow.models.dag import DAG

from airflow.operators.bash import BashOperator
from airflow.models import Variable

from src import cellar_helpers as helpers

default_args = {
    "depends_on_past": False,
    "email_on_failure": False,
    "email_on_retry": False,
    "retries": 0,
    "retry_delay": timedelta(minutes=5),
}

HARVEST_CELLAR_IMAGE = Variable.get(
    "HARVEST_CELLAR_IMAGE",
    default_var="code.europa.eu:4567/seta/v3/data/harvest/harvest-cellar:latest",
)

MEDIA_PATH = Variable.get("MEDIA_PATH", default_var="/media")
SETA_NETWORK = Variable.get("SETA_NETWORK", default_var="seta-network")


with DAG(
    "harvest-cellar-dag",
    default_args=default_args,
    description="Harvest cellar publications",
    schedule=None,
    start_date=datetime(2024, 1, 1),
    catchup=False,
    tags=["harvest", "cellar"],
) as dag:

    t_sleep = BashOperator(
        task_id="sleep",
        depends_on_past=False,
        bash_command="sleep 5",
    )

    t_download = helpers.create_download_operator(
        task_id="download_cellar_ENG",
        image=HARVEST_CELLAR_IMAGE,
        container_name="task___download_cellar_ENG",
        media_path=MEDIA_PATH,
        seta_network=SETA_NETWORK,
        lang="eng",
        langISO2="en",
    )

    t_format = helpers.create_formatter_operator(
        task_id="cellar_formatter",
        image=HARVEST_CELLAR_IMAGE,
        container_name="task___cellar_formatter",
        media_path=MEDIA_PATH,
        seta_network=SETA_NETWORK,
    )

    t_download >> t_sleep >> t_format  # pylint: disable=pointless-statement
