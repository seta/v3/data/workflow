"""
Check docker operator documentation: 
https://airflow.apache.org/docs/apache-airflow-providers-docker/stable/_api/airflow/providers/docker/operators/docker/index.html
"""

from datetime import datetime, timedelta
from airflow.models.dag import DAG

from airflow.providers.docker.operators.docker import DockerOperator, Mount
from airflow.models import Variable

default_args = {
    "depends_on_past": False,
    "email_on_failure": False,
    "email_on_retry": False,
    "retries": 0,
    "retry_delay": timedelta(minutes=5),
}

HARVEST_CELLAR_IMAGE = Variable.get(
    "HARVEST_CELLAR_IMAGE",
    default_var="code.europa.eu:4567/seta/v3/data/harvest/harvest-cellar:latest",
)

MEDIA_PATH = Variable.get("MEDIA_PATH", default_var="/media")
SETA_NETWORK = Variable.get("SETA_NETWORK", default_var="seta-network")

with DAG(
    "import-cellar-eng-dag",
    default_args=default_args,
    description="Import cellar publications",
    schedule=None,
    start_date=datetime(2024, 1, 1),
    catchup=False,
    tags=["import", "cellar"],
) as dag:

    t_import_ENG = DockerOperator(
        task_id="import_cellar_ENG",
        image=HARVEST_CELLAR_IMAGE,
        container_name="task___import_cellar_ENG",
        auto_remove=True,
        command="import.py --lang eng --langISO2 en --import_dir {{ var.value.get('IMPORT_CELLAR_ENG_PATH', '/media/backup/cellar/') }}",
        network_mode=SETA_NETWORK,
        mount_tmp_dir=False,
        mounts=[Mount(source=MEDIA_PATH, target="/media", type="bind")],
        environment={
            "LOG_LEVEL": "{{ var.value.get('LOG_LEVEL') }}",
            "NLP_API_URL": "{{ var.value.get('NLP_API_URL') }}",
            "LOGS_FILE_PATH": "{{ var.value.get('LOGS_IMPORT_FILE_PATH') }}",
        },
    )

    t_import_ENG  # pylint: disable=pointless-statement
