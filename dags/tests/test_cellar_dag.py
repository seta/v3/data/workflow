from datetime import datetime, timedelta
from airflow.models.dag import DAG

from airflow.providers.docker.operators.docker import DockerOperator, Mount
from airflow.operators.bash import BashOperator
from airflow.models import Variable

default_args = {
    "depends_on_past": False,
    "email_on_failure": False,
    "email_on_retry": False,
    "retries": 0,
    "retry_delay": timedelta(minutes=5),
    # 'queue': 'bash_queue',
    # 'pool': 'backfill',
    # 'priority_weight': 10,
    # 'end_date': datetime(2016, 1, 1),
    # 'wait_for_downstream': False,
    # 'sla': timedelta(hours=2),
    # 'execution_timeout': timedelta(seconds=300),
    # 'on_failure_callback': some_function, # or list of functions
    # 'on_success_callback': some_other_function, # or list of functions
    # 'on_retry_callback': another_function, # or list of functions
    # 'sla_miss_callback': yet_another_function, # or list of functions
    # 'on_skipped_callback': another_function, #or list of functions
    # 'trigger_rule': 'all_success'
}

HARVEST_CELLAR_IMAGE = Variable.get(
    "HARVEST_CELLAR_IMAGE",
    default_var="code.europa.eu:4567/seta/v3/data/harvest/harvest-cellar:latest",
)
MEDIA_PATH = Variable.get("MEDIA_PATH", default_var="/media")
SETA_NETWORK = Variable.get("SETA_NETWORK", default_var="seta-network")

IMPORT_CELLAR_ENG_PATH = Variable.get(
    "IMPORT_CELLAR_ENG_PATH", default_var="/media/backup/cellar/"
)


with DAG(
    "test-harvest-cellar-dag",
    default_args=default_args,
    description="Test harvest cellar publications",
    schedule=None,
    start_date=datetime(2024, 1, 1),
    catchup=False,
    tags=["test", "harvest", "cellar"],
) as dag:

    t_sleep = BashOperator(
        task_id="sleep",
        depends_on_past=False,
        bash_command="sleep 5",
    )

    t_download_ENG = DockerOperator(
        task_id="download_cellar_ENG",
        image=HARVEST_CELLAR_IMAGE,
        container_name="task___test_download_cellar_ENG",
        auto_remove=True,
        command="download.py --lang eng --langISO2 en --test yes",
        network_mode=SETA_NETWORK,
        mount_tmp_dir=False,
        mounts=[Mount(source=MEDIA_PATH, target="/media", type="bind")],
        environment={
            "LOG_LEVEL": "{{ var.value.get('LOG_LEVEL') }}",
            "NLP_API_URL": "{{ var.value.get('NLP_API_URL') }}",
            "LOGS_FILE_PATH": "{{ var.value.get('LOGS_HARVEST_FILE_PATH') }}",
            "NO_PROXY": "nlp",
            "HTTP_PROXY": "{{ var.value.get('HTTP_PROXY_SECRET', '') }}",
            "HTTPS_PROXY": "{{ var.value.get('HTTPS_PROXY_SECRET', '') }}",
        },
    )

    t_format = DockerOperator(
        task_id="cellar_formatter",
        image=HARVEST_CELLAR_IMAGE,
        container_name="task___test_cellar_formatter",
        auto_remove=True,
        command="format_ingest.py --test yes",
        network_mode=SETA_NETWORK,
        mount_tmp_dir=False,
        mounts=[Mount(source=MEDIA_PATH, target="/media", type="bind")],
        environment={
            "LOG_LEVEL": "{{ var.value.get('LOG_LEVEL') }}",
            "NLP_API_URL": "{{ var.value.get('NLP_API_URL') }}",
            "LOGS_FILE_PATH": "{{ var.value.get('LOGS_FORMATTER_FILE_PATH') }}",
            "NO_PROXY": "nlp",
            "HTTP_PROXY": "{{ var.value.get('HTTP_PROXY_SECRET', '') }}",
            "HTTPS_PROXY": "{{ var.value.get('HTTPS_PROXY_SECRET', '') }}",
        },
    )

    t_download_ENG >> t_sleep >> t_format  # pylint: disable=pointless-statement
