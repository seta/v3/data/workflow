from airflow.providers.docker.operators.docker import DockerOperator, Mount


def create_download_operator(
    task_id: str,
    image: str,
    container_name: str,
    media_path: str,
    seta_network: str,
    **kwargs,
) -> DockerOperator:
    """Create a DockerOperator to download cellar publications.

    kwargs:
        --lang Download language, "eng", "spa", "dan", "deu", "fra", "ita", "nld", "nor", "por", "swe", etc.
        --langISO2 Download language, "en", "es", "da", "de", "fr", "it", "nl", "no", "pt", "sv", etc.
        --resume Resume file download for language from the last metadata import, yes/no - no by default.
        --test Run in test mode, yes/no - no by default.
    """

    cmd = "download.py"
    for key, value in kwargs.items():
        cmd += f" --{key} {value}"

    return DockerOperator(
        task_id=task_id,
        image=image,
        container_name=container_name,
        auto_remove=True,
        command=cmd,
        network_mode=seta_network,
        mount_tmp_dir=False,
        mounts=[Mount(source=media_path, target="/media", type="bind")],
        environment={
            "LOG_LEVEL": "{{ var.value.get('LOG_LEVEL') }}",
            "NLP_API_URL": "{{ var.value.get('NLP_API_URL') }}",
            "LOGS_FILE_PATH": "{{ var.value.get('LOGS_HARVEST_FILE_PATH') }}",
            "NO_PROXY": "nlp",
            "HTTP_PROXY": "{{ var.value.get('HTTP_PROXY_SECRET', '') }}",
            "HTTPS_PROXY": "{{ var.value.get('HTTPS_PROXY_SECRET', '') }}",
        },
    )


def create_formatter_operator(
    task_id: str,
    image: str,
    container_name: str,
    media_path: str,
    seta_network: str,
    **kwargs,
) -> DockerOperator:
    """Create a DockerOperator to format cellar publications."""

    cmd = "format_ingest.py"
    if kwargs:
        for key, value in kwargs.items():
            cmd += f" --{key} {value}"

    return DockerOperator(
        task_id=task_id,
        image=image,
        container_name=container_name,
        auto_remove=True,
        command=cmd,
        network_mode=seta_network,
        mount_tmp_dir=False,
        mounts=[Mount(source=media_path, target="/media", type="bind")],
        environment={
            "LOG_LEVEL": "{{ var.value.get('LOG_LEVEL') }}",
            "NLP_API_URL": "{{ var.value.get('NLP_API_URL') }}",
            "LOGS_FILE_PATH": "{{ var.value.get('LOGS_FORMATTER_FILE_PATH') }}",
            "NO_PROXY": "nlp",
            "HTTP_PROXY": "{{ var.value.get('HTTP_PROXY_SECRET', '') }}",
            "HTTPS_PROXY": "{{ var.value.get('HTTPS_PROXY_SECRET', '') }}",
        },
    )
