"""
Check docker operator documentation: 
https://airflow.apache.org/docs/apache-airflow-providers-docker/stable/_api/airflow/providers/docker/operators/docker/index.html
"""

from datetime import datetime, timedelta
from airflow.models.dag import DAG

from airflow.providers.docker.operators.docker import DockerOperator, Mount
from airflow.models import Variable

default_args = {
    "depends_on_past": False,
    "email_on_failure": False,
    "email_on_retry": False,
    "retries": 0,
    "retry_delay": timedelta(minutes=5),
}

INGEST_IMAGE = Variable.get(
    "INGEST_IMAGE",
    default_var="code.europa.eu:4567/seta/v3/data/ingest-service:latest",
)
MEDIA_PATH = Variable.get("MEDIA_PATH", default_var="/media")
SETA_NETWORK = Variable.get("SETA_NETWORK", default_var="seta-network")

PRIVATE_KEY_FOLDER_PATH = Variable.get(
    "PRIVATE_KEY_FOLDER_PATH", default_var="/private-keys/"
)

with DAG(
    "ingest-dag",
    default_args=default_args,
    description="Ingest formatted data into the SETA platform",
    schedule=None,
    start_date=datetime(2024, 1, 1),
    catchup=False,
    tags=["ingest"],
) as dag:

    t_format = DockerOperator(
        task_id="ingest",
        image=INGEST_IMAGE,
        container_name="task___ingest",
        auto_remove=True,
        command="ingest.py",
        network_mode="host",
        mount_tmp_dir=False,
        mounts=[
            Mount(source=MEDIA_PATH, target="/media", type="bind"),
            Mount(
                source=PRIVATE_KEY_FOLDER_PATH, target="/home/seta/private", type="bind"
            ),
        ],
        environment={
            "LOG_LEVEL": "{{ var.value.get('LOG_LEVEL') }}",
            "AUTH_URL": "{{ var.value.get('AUTH_URL') }}",
            "SEARCH_API_URL": "{{ var.value.get('SEARCH_API_URL') }}",
        },
    )

    t_format  # pylint: disable=pointless-statement
